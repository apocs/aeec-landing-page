<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_breadcrumbs
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
$menu    = JFactory::getApplication()->getMenu();
$id    =   $menu->getActive()->id;

/** Getting the Title of the Menu by using id. **/ 
$db    = JFactory::getDBO();
$query    = "SELECT * FROM #__menu WHERE id = '$id'";
$db->setQuery($query);
$rows    = $db->loadObjectList();
$itemrow = $rows[0];
$title   =   $itemrow->title;
$des   =   $itemrow->note;
?>

<h1><?php echo $title; ?></h1>
<span><?php echo $des; ?></span>


<ul class="crumb <?php echo $moduleclass_sfx; ?>">
	<?php
	if ($params->get('showHere', 1))
	{
		echo '<li class="active">' . JText::_('MOD_BREADCRUMBS_HERE') . '&#160;</li>';
	}
	

	// Get rid of duplicated entries on trail including home page when using multilanguage
	for ($i = 0; $i < $count; $i++)
	{
		if ($i == 1 && !empty($list[$i]->link) && !empty($list[$i - 1]->link) && $list[$i]->link == $list[$i - 1]->link)
		{
			unset($list[$i]);
		}
	}

	// Find last and penultimate items in breadcrumbs list
	end($list);
	$last_item_key = key($list);
	prev($list);
	$penult_item_key = key($list);

	// Make a link if not the last item in the breadcrumbs
	$show_last = $params->get('showLast', 1);

	// Generate the trail
	foreach ($list as $key => $item) :
	if ($key != $last_item_key)
	{
		// Render all but last item - along with separator
		echo '<li>';
		if (!empty($item->link))
		{
			echo '<a href="' . $item->link . '" class="pathway">' . $item->name . '</a>';
		}
		else
		{
			echo $item->name;
		}
		echo '</li>';
		
		if (($key != $penult_item_key) || $show_last)
		{
			echo '<li class="sep">' . $separator . '</li>';
		}

		
	}
	elseif ($show_last)
	{
		// Render last item if reqd.
		
		echo '<li>' . $item->name . '</li>';
		
	}
	endforeach; ?>
</ul>
