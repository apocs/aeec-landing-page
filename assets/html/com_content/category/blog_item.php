<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Create a shortcut for params.
$params = $this->item->params;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
$canEdit = $this->item->params->get('access-edit');
$info    = $params->get('info_block_position', 0);
?>
<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
	|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != '0000-00-00 00:00:00' )) : ?>
	<div class="system-unpublished">
<?php endif; ?>
<!-- Post -->
			<li>
							<?php if ($params->get('show_publish_date')) : ?>								
							<div class="date-box"><span class="day"><?php echo JText::sprintf( JHtml::_('date', $this->item->publish_up, JText::_('d'))); ?></span> <span class="month"><?php echo JText::sprintf( JHtml::_('date', $this->item->publish_up, JText::_('M'))); ?></span> </div>
							<?php endif; ?>
                            <div class="post-content">
                                <div class="post-image">
                                		<div class="owl-arrow">
                                        	<span class="prev"></span>
                                        	<span class="next"></span>
                                        </div>
                                        <div class="blog-slide">
                                        	<?php echo JLayoutHelper::render('joomla.content.intro_image', $this->item); ?>
                                        </div>
                                </div>


                                <div class="post-text">
                                    <h3><?php echo JLayoutHelper::render('joomla.content.blog_style_default_item_title', $this->item); ?></h3>
									<?php echo $this->item->introtext; ?>
								</div>
                            </div>
                            <div class="post-meta">
							<?php if ($params->get('show_author')) : ?>
							<span>
								<?php $author = $this->item->created_by_alias ? $this->item->created_by_alias : $this->item->author; ?>
								<?php $author = '<span>' . $author . '</span>'; ?>
								<?php if (!empty($this->item->contact_link) && $params->get('link_author') == true) : ?>
								<i class="icon-user"></i>By: <?php echo JText::sprintf(JHtml::_('link', $this->item->contact_link, $author, array('itemprop' => 'url'))); ?>
								<?php else: ?>
							    <i class="icon-user"></i>By: <?php echo JText::sprintf($author); ?>
								<?php endif; ?>
							</span> 
								<?php endif; ?>
								<?php if ($params->get('show_category')) : ?>
							<span>
									<?php $title = $this->escape($this->item->category_title); ?>
									<?php if ($params->get('link_category') && $this->item->catslug) : ?>
										<i class="icon-tag"></i><?php $url = '<a href="' . JRoute::_(ContentHelperRoute::getCategoryRoute($this->item->catslug)) . '" itemprop="genre">' . $title . '</a>'; ?>
										<?php echo JText::sprintf($url); ?>
									<?php else : ?>
										<i class="icon-tag"></i><?php echo JText::sprintf('<span>' . $title . '</span>'); ?>
									<?php endif; ?>
							</span> 
								<?php endif; ?>
							<?php if ($params->get('show_hits')) : ?>
							<span>
									<a href="#">
										<meta itemprop="interactionCount" content="UserPageVisits:<?php echo $this->item->hits; ?>" />
									<i class="icon-comment"></i> <?php echo JText::sprintf('COM_CONTENT_ARTICLE_HITS', $this->item->hits); ?>
									</a>
							</span> 
								<?php endif; ?>
								<?php if ($params->get('show_modify_date')) : ?>
							<span>
								Modified on: <?php echo JText::sprintf( JHtml::_('date', $this->item->publish_up, JText::_('M d Y'))); ?>		
							</span>
								<?php endif; ?>
								<?php if ($params->get('show_create_date')) : ?>
							<span>
								Created on: <?php echo JText::sprintf( JHtml::_('date', $this->item->publish_up, JText::_('M d Y'))); ?>
							</span>
								<?php endif; ?>
								<?php if ($params->get('show_parent_category')) : ?>
							<span>
									<?php $title = $this->escape($this->item->parent_title); ?>
									<?php if ($params->get('link_parent_category') && !empty($this->item->parent_slug)) : ?>
									<?php $url = '<a href="' . JRoute::_(ContentHelperRoute::getCategoryRoute($this->item->parent_slug)) . '" itemprop="genre">' . $title . '</a>'; ?>
									<?php echo JText::sprintf($url); ?>
									<?php else : ?>
									<?php echo JText::sprintf('<span>' . $title . '</span>'); ?>
									<?php endif; ?>
							</span>
								<?php endif; ?>
							<?php if ($params->get('show_readmore') && $this->item->readmore) :
										if ($params->get('access-view')) :
											$link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
										else :
											$menu = JFactory::getApplication()->getMenu();
											$active = $menu->getActive();
											$itemId = $active->id;
											$link1 = JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId);
											$returnURL = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid));
											$link = new JUri($link1);
											$link->setVar('return', base64_encode($returnURL));
										endif; ?>

							<span>
										<i class="icon-arrow-right"></i> <?php echo JLayoutHelper::render('joomla.content.readmore', array('item' => $this->item, 'params' => $params, 'link' => $link)); ?>
							</span> 
									<?php endif; ?>
							</div>
                        
				
				
			</li>








<?php if ($this->item->state == 0 || strtotime($this->item->publish_up) > strtotime(JFactory::getDate())
	|| ((strtotime($this->item->publish_down) < strtotime(JFactory::getDate())) && $this->item->publish_down != '0000-00-00 00:00:00' )) : ?>
</div>
<?php endif; ?>

<?php echo $this->item->event->afterDisplayContent; ?>
