<?php
/**
 * @version		$Id: tag.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<!-- Start K2 Tag Layout -->
<div id="k2Container" class="tagView<?php if($this->params->get('pageclass_sfx')) echo ' '.$this->params->get('pageclass_sfx'); ?>">

	<?php if($this->params->get('show_page_title')): ?>
	<!-- Page title -->
	<div class="componentheading<?php echo $this->params->get('pageclass_sfx')?>">
		<?php echo $this->escape($this->params->get('page_title')); ?>
	</div>
	<?php endif; ?>


	<?php if(count($this->items)): ?>
	<ul class="blog-list">
		<?php foreach($this->items as $item): ?>
		
		<li>
		<?php if($item->params->get('tagItemDateCreated',1)): ?>
        <div class="date-box"><span class="day"><?php echo JHTML::_('date', $item->created , JText::_('d')); ?></span> <span class="month"><?php echo JHTML::_('date', $item->created , JText::_('M')); ?></span> </div>
		<?php endif; ?>
            <div class="post-content">
				<?php if($item->params->get('tagItemImage',1) && !empty($item->imageGeneric)): ?>
				<div class="post-image">

					<div class="blog-slide">
			  <!-- Item Image -->
			 	    <a href="<?php echo $item->link; ?>" title="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>">
				    	<img src="<?php echo $item->imageGeneric; ?>" alt="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>" style="width:100%; height:auto;" />
				    </a>
					</div>
				</div>
			  <?php endif; ?>


				<div class="post-text">
				<?php if($item->params->get('tagItemTitle',1)): ?>
				<!-- Item title -->
				<h3>
			  	<?php if ($item->params->get('tagItemTitleLinked',1)): ?>
					<a href="<?php echo $item->link; ?>">
			  		<?php echo $item->title; ?>
			  	</a>
			  	<?php else: ?>
			  	<?php echo $item->title; ?>
			  	<?php endif; ?>
				</h3>
				<?php endif; ?>
				<?php if($item->params->get('tagItemIntroText',1)): ?>
				<!-- Item introtext -->
			  	<?php echo $item->introtext; ?>
				<?php endif; ?>
				</div>
            </div>
        <div class="post-meta">
			
			<?php if($item->params->get('tagItemCategory')): ?>
			<!-- Item category name -->
			<span><i class="fa fa-book"></i>
				<a href="<?php echo $item->category->link; ?>"><?php echo $item->category->name; ?></a>
			</span>
			<?php endif; ?> 
			<?php if ($item->params->get('tagItemReadMore')): ?>
			<!-- Item "read more..." link -->
			<span><i class="fa fa-arrow-right"></i>
				<a href="<?php echo $item->link; ?>">
					<?php echo JText::_('K2_READ_MORE'); ?>
				</a>
			</span>
			<?php endif; ?>
		</div>
		
		<?php if($item->params->get('tagItemExtraFields',0) && count($item->extra_fields)): ?>
		  <!-- Item extra fields -->  
		  <div class="tagItemExtraFields">
		  	<h4><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></h4>
		  	<ul>
				<?php foreach ($item->extra_fields as $key=>$extraField): ?>
				<?php if($extraField->value != ''): ?>
				<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
					<?php if($extraField->type == 'header'): ?>
					<h4 class="tagItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
					<?php else: ?>
					<span class="tagItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
					<span class="tagItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
					<?php endif; ?>		
				</li>
				<?php endif; ?>
				<?php endforeach; ?>
				</ul>
		    <div class="clr"></div>
		  </div>
		  <?php endif; ?>
        </li>
		<?php endforeach; ?>
	</ul>

	<!-- Pagination -->
	<?php if($this->pagination->getPagesLinks()): ?>
	<div class="k2Pagination">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<div class="clr"></div>
		<?php echo $this->pagination->getPagesCounter(); ?>
	</div>
	<?php endif; ?>

	<?php endif; ?>
	
</div>
<!-- End K2 Tag Layout -->
