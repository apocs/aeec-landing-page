<?php
/**
 * @version		$Id: user.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Get user stuff (do not change)
$user = JFactory::getUser();

?>

<!-- Start K2 User Layout -->

<div id="k2Container" class="userView<?php if($this->params->get('pageclass_sfx')) echo ' '.$this->params->get('pageclass_sfx'); ?>">

	<?php if($this->params->get('show_page_title') && $this->params->get('page_title')!=$this->user->name): ?>
	<!-- Page title -->
	<div class="componentheading<?php echo $this->params->get('pageclass_sfx')?>">
		<?php echo $this->escape($this->params->get('page_title')); ?>
	</div>
	<?php endif; ?>

	
	
 <?php if ($this->params->get('userImage') || $this->params->get('userName') || $this->params->get('userDescription') || $this->params->get('userURL') || $this->params->get('userEmail')): ?>
  <!-- Author Block -->
  <div class="itemAuthorBlock">
	<?php if ($this->params->get('userImage') && !empty($this->user->avatar)): ?>
		<img class="itemAuthorAvatar" src="<?php echo $this->user->avatar; ?>" alt="<?php echo $this->user->name; ?>" style="width:<?php echo $this->params->get('userImageWidth'); ?>px; height:auto;" />
	<?php endif; ?>
		
  	

    <div class="itemAuthorDetails">
	<?php if ($this->params->get('userName')): ?>
      <h3 class="itemAuthorName">
		<?php echo $this->user->name; ?>
      </h3>
	<?php endif; ?>
    
	<?php if ($this->params->get('userDescription') && trim($this->user->profile->description)): ?>
		<p><?php echo $this->user->profile->description; ?></p>
	<?php endif; ?>
		
	<?php if (($this->params->get('userURL') && isset($this->user->profile->url) && $this->user->profile->url) || $this->params->get('userEmail')): ?>
			<?php if ($this->params->get('userURL') && isset($this->user->profile->url) && $this->user->profile->url): ?>
			<span class="itemAuthorUrl">
				<?php echo JText::_('K2_WEBSITE_URL'); ?>: <a href="<?php echo $this->user->profile->url; ?>" target="_blank" rel="me"><?php echo $this->user->profile->url; ?></a>
			</span>
			<?php endif; ?>

			<?php if ($this->params->get('userEmail')): ?>
			<span class="itemAuthorEmail">
				<?php echo JText::_('K2_EMAIL'); ?>: <?php echo JHTML::_('Email.cloak', $this->user->email); ?>
			</span>
			<?php endif; ?>	
		
	<?php endif; ?>
		
    <div class="clr"></div>
		<?php echo $this->user->event->K2UserDisplay; ?>	
	<div class="clr"></div>
    </div>
  </div>
  <?php endif; ?>
<div class="clr"></div>
	

	<?php if(count($this->items)): ?>
	<!-- Item list -->
	<ul class="blog-list">
		<?php foreach ($this->items as $item): ?>
		<li>
		<?php if($item->params->get('userItemDateCreated',1)): ?>
        <div class="date-box"><span class="day"><?php echo JHTML::_('date', $item->created , JText::_('d')); ?></span> <span class="month"><?php echo JHTML::_('date', $item->created , JText::_('M')); ?></span> </div>
		<?php endif; ?>
            <div class="post-content">
				<?php if($item->params->get('userItemImage',1) && !empty($item->imageGeneric)): ?>
				<div class="post-image">

					<div class="blog-slide">
			  <!-- Item Image -->
			 	    <a href="<?php echo $item->link; ?>" title="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>">
				    	<img src="<?php echo $item->imageGeneric; ?>" alt="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>" style="width:100%; height:auto;" />
				    </a>
					</div>
				</div>
			  <?php endif; ?>


				<div class="post-text">
				<?php if($item->params->get('userItemTitle',1)): ?>
				<!-- Item title -->
				<h3>
			  	<?php if ($item->params->get('userItemTitleLinked',1)): ?>
					<a href="<?php echo $item->link; ?>">
			  		<?php echo $item->title; ?>
			  	</a>
			  	<?php else: ?>
			  	<?php echo $item->title; ?>
			  	<?php endif; ?>
				</h3>
				<?php endif; ?>
				<?php if($item->params->get('userItemIntroText',1)): ?>
				<!-- Item introtext -->
			  	<?php echo $item->introtext; ?>
				<?php endif; ?>
				</div>
            </div>
        <div class="post-meta">
			
			<?php if($item->params->get('userItemCategory')): ?>
			<!-- Item category name -->
			<span><i class="fa fa-book"></i>
				<a href="<?php echo $item->category->link; ?>"><?php echo $item->category->name; ?></a>
			</span>
			<?php endif; ?> 
			
			<?php if($this->params->get('userItemCommentsAnchor') && ( ($this->params->get('comments') == '2' && !$this->user->guest) || ($this->params->get('comments') == '1')) ): ?>
			<!-- Anchor link to comments below -->
				<?php if(!empty($item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($item->numOfComments > 0): ?>
					<a href="<?php echo $item->link; ?>#itemCommentsAnchor">
						<span><i class="fa fa-comments"></i><?php echo $item->numOfComments; ?> <?php echo ($item->numOfComments>1) ? JText::_('K2_COMMENTS') : JText::_('K2_COMMENT'); ?></span>
					</a>
					<?php else: ?>
					<a href="<?php echo $item->link; ?>#itemCommentsAnchor">
						<span><i class="fa fa-comment"></i><?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?></span>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
			
			<?php if ($item->params->get('userItemReadMore')): ?>
			<!-- Item "read more..." link -->
			<span><i class="fa fa-arrow-right"></i>
				<a href="<?php echo $item->link; ?>">
					<?php echo JText::_('K2_READ_MORE'); ?>
				</a>
			</span>
			<?php endif; ?>
		</div>
		
		<?php if($item->params->get('userItemExtraFields',0) && count($item->extra_fields)): ?>
		  <!-- Item extra fields -->  
		  <div class="userItemExtraFields">
		  	<h4><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></h4>
		  	<ul>
				<?php foreach ($item->extra_fields as $key=>$extraField): ?>
				<?php if($extraField->value != ''): ?>
				<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
					<?php if($extraField->type == 'header'): ?>
					<h4 class="tagItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
					<?php else: ?>
					<span class="tagItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
					<span class="tagItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
					<?php endif; ?>		
				</li>
				<?php endif; ?>
				<?php endforeach; ?>
				</ul>
		    <div class="clr"></div>
		  </div>
		  <?php endif; ?>
		  
        </li>
		
			
		<?php endforeach; ?>
	</ul>

	<!-- Pagination -->
	<?php if(count($this->pagination->getPagesLinks())): ?>
	<div class="k2Pagination">
		<?php echo $this->pagination->getPagesLinks(); ?>
		<div class="clr"></div>
		<?php echo $this->pagination->getPagesCounter(); ?>
	</div>
	<?php endif; ?>
	
	<?php endif; ?>

</div>

<!-- End K2 User Layout -->
